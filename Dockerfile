#Imagem adicionada ao DockerHub para facilitar seus testes :) 
#https://hub.docker.com/repository/docker/gmanera/zenvia-cv
#DockerFile utilizando webserver (NGINX) para disponibilização de serviço de CV via HTTPS (SSL/TSL).
FROM scratch AS zenvia
MAINTAINER Gabriel Manera "gabriel.manera@hotmail.com"
FROM nginx:latest

##Limpeza dos arquivos default e copia arquivos para conteudo do CV
RUN rm -rf /var/www/html/*;

#Encaminhamento de CV e configuração webserver
#Script para inicialização automatizada do Nginx
COPY ["cv.html","/usr/share/nginx/html/cv.html"]
COPY ["default.conf","/etc/nginx/conf.d/default.conf"]
COPY ["start.sh", "/root/start.sh"]

#Copia Certificados para /etc/nginx/ssl/ e contorno do bug: https://github.com/openssl/openssl/issues/7754
RUN mkdir -p /etc/nginx/ssl && touch ~/.rnd 

#Geração dos certificados com duração de 1 ano (o certificado será criado apenas uma vez, durante a criação da imagem).
RUN openssl genrsa -des3 -passout pass:amdhd5770 -out /etc/nginx/ssl/zenvia.pass.key 2048 && \
    openssl rsa -passin pass:amdhd5770 -in /etc/nginx/ssl/zenvia.pass.key -out /etc/nginx/ssl/zenvia.key && \
    rm /etc/nginx/ssl/zenvia.pass.key && \
    openssl req -new -key /etc/nginx/ssl/zenvia.key -out /etc/nginx/ssl/zenvia.csr \
        -subj "/C=BR/ST=RS/L=Canoas/O=Zenvia/OU=RH/CN=cv-zenvia.test" && \
    openssl x509 -req -days 365 -in /etc/nginx/ssl/zenvia.csr -signkey /etc/nginx/ssl/zenvia.key -out /etc/nginx/ssl/zenvia.crt

# Direciona os logs para o Docker log collector (https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-docker/)
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log

#Destinos dos volumes
VOLUME ["/usr/share/nginx/html"]
VOLUME ["/etc/nginx"]
VOLUME ["/var/cache/nginx"]

#Exposição apenas da porta 443
EXPOSE 443 

#https://docs.docker.com/engine/reference/commandline/stop/
STOPSIGNAL SIGTERM

#startup
WORKDIR /root
CMD ["./start.sh"]
CMD ["nginx", "-g", "daemon off;"]