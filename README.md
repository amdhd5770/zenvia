# Desafio #1
Estrutura para publicação de CV em um ambiente Kubernetes e Docker.

Desenvolvido ao som de https://open.spotify.com/playlist/37i9dQZF1DXd6tJtr4qeot?si=bobzpSRuTQm5vFVQhinKcw

## Imagem publicada no Docker Hub:
Imagem publicada no Docker Hub para facilitar a avaliação do código. [zenvia-cv](https://hub.docker.com/repository/docker/gmanera/zenvia-cv).
```bash
docker push gmanera/zenvia-cv:latest
```

## Arquivos disponibilizados:
[cv.html](https://bitbucket.org/amdhd5770/zenvia/src/master/cv.html) - Arquivo HTML para publicação do curriculo em formato adequado.

[default.conf](https://bitbucket.org/amdhd5770/zenvia/src/master/default.conf) - Arquivo de configuração default para o webserver utilizado na imagem (Nginx).

[Dockerfile](https://bitbucket.org/amdhd5770/zenvia/src/master/Dockerfile) Arquivo com a receita para construir o container.

[Namespace.yml](https://bitbucket.org/amdhd5770/zenvia/src/master/Namespace.yml) - Arquivo para criação do namespace.

[Service.yml](https://bitbucket.org/amdhd5770/zenvia/src/master/Service.yml) - Arquivo para exposição do serviço externamente utilizando LoadBalancer na porta 443.

[Deployment.yml](https://bitbucket.org/amdhd5770/zenvia/src/master/Deployment.yml) - Arquivo contemplando os requisitos de 3 replicas (mínimo) e limite de recursos.

[start.sh](https://bitbucket.org/amdhd5770/zenvia/src/master/start.sh) - Arquivo para inicialização automatizada de serviços internamente na imagem.

## Código versionado conforme solicitado no enunciado do Desafio #1
Projeto: [zenvia](https://bitbucket.org/amdhd5770/zenvia/src/master/).
```bash
git clone https://gmanera@bitbucket.org/amdhd5770/zenvia.git
```
## Docker Run
Em caso de necessidade, para subirmos apenas a imagem no Docker, pode-se usar os comandos abaixo:
```bash
docker build -t "gmanera/zenvia-cv:latest" .
docker run --name zenvia-cv -p 443:443 -d gmanera/zenvia-cv:latest
```

## Objetos K8S
Para facilitar, segue método de simples criação dos objetos no Kubernetes
```bash
cat << EOF | kubectl create -f -
---
apiVersion: v1
kind: Namespace
metadata:
  name: zenvia
EOF

cat << EOF | kubectl create -f -
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: zenvia-cv-deployment2
  namespace: zenvia
spec:
  selector:
    matchLabels:
      app: zenvia-cv
  replicas: 3
  template:
    metadata:
      labels:
        app: zenvia-cv
    spec:
      containers:
      - name: zenvia-cv
        image: gmanera/zenvia-cv:latest
        resources:
          requests:
            memory: "64Mi"
            cpu: "250m"
          limits:
            memory: "128Mi"
            cpu: "500m"
        ports:
        - containerPort: 443
EOF
cat << EOF | kubectl create -f -
---
apiVersion: v1
kind: Service
metadata:
  name: zenvia-cv
  namespace: zenvia
  labels:
    app: zenvia-cv
spec:
  type: LoadBalancer
  ports:
  - name: https
    port: 443
    targetPort: https
    protocol: TCP
  selector:
    app: zenvia-cv
EOF
```
